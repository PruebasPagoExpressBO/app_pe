/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  Button,
} from 'react-native';

import axios from 'axios';

export default function ServiceScreen({ route, navigation }) {
  const [services, setServices] = useState([]);

  React.useEffect(() => {
    const fetchData = async () => {
      const response = await axios({
        method: 'post',
        url: 'http://200.115.16.18:8280/TrexWS/pagossrl_testing/api/servicios',
        data: {},
        headers: {
          Authorization:
            'Basic cGFnb3Nzcmw6ZDlmMjY3MmEzNDBmMDVlY2YyOWE2MjBmZmZkNzllYjE=',
          'Timestamp-Date': 'Tue, 22 Sep 2020 23:10:45 GMT',
          'Content-Type': 'application/json',
        },
      });

      setServices(response.data.datos);
    };

    fetchData();
  }, []);

  return (
    <View>
      <Text>Hello World...!</Text>
      <FlatList
        contentContainerStyle={{paddingBottom: 30}}
        data={services}
        renderItem={({item}) => <View>{showServices(item)}</View>}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );

  function showServices(item) {
    return (
      <View>
        <Button
          title={item.descripcion}
          onPress={() => navigation.navigate("Query", { servicio: item.servicio })}></Button>
      </View>
    );
  }
}
