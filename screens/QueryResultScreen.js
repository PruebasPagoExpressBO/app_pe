import React, {useState} from 'react';
import {Text, View, TextInput, ScrollView, Button} from 'react-native';

import axios from 'axios';

export default function QueryResultScreen({route, navigation}) {
  const {campos, servicio, confirmacion} = route.params;

  const [result, setResult] = useState([]);
  const [indexButton, setIndexButton] = useState(0);

  React.useEffect(() => {
    const fetchData = async () => {
      const response = await axios({
        method: 'post',
        url:
          'http://200.115.16.18:8280/TrexWS/pagossrl_testing/api/transacciones/consulta',
        data: {
          servicio: servicio,
          campos: JSON.parse(campos),
        },
        headers: {
          Authorization:
            'Basic cGFnb3Nzcmw6MDcwZWM1OTQzZWVjMGQwN2I1YzdlZDhjMTA0YmE0YjM=',
          'Timestamp-Date': 'Thu, 24 Sep 2020 23:10:37 GMT',
          'Content-Type': 'application/json',
        },
      });

      setResult(response.data.datos);
    };

    fetchData();
  }, []);

  const mapping = () => {
    return result.forEach((obj) => {  
      return (
        <View style={{borderWidth: 5}}>
          {mappingResult()}
          {renderButton()}
        </View>
      );
    });
  };

  const renderButton = () => {
    if (indexButton === 0) {
      setIndexButton(indexButton + 1);
      return <Button title="Payment"></Button>;
    }
    
    setIndexButton(indexButton + 1);
  };

  const mappingResult = () => {
    return confirmacion.map((obj) => {
      // Deconstruct key and title to use
      const {parametro, etiqueta, editable, oculto} = obj;
      let value = null;

      let componentEtiqueta;

      if (!oculto) {
        componentEtiqueta = <Text>{etiqueta}:</Text>;
        value = searchValue(parametro, editable, etiqueta);
      }

      // return components
      return (
        <View>
          {componentEtiqueta}
          {value}
        </View>
      );
    });
  };

  const searchValue = (key, editable, etiqueta) => {
    let value = '';
    for (let i = 0; i < result.length; i = i + 1) {
      let camposResult = result[i]['campos'];

      value = camposResult[key];
    }

    let componentValue;

    if (editable) {
      let placeholder = value.length <= 0 ? etiqueta : '';
      placeholder;
      componentValue = (
        <TextInput placeholder={placeholder} value={value}></TextInput>
      );
    } else {
      componentValue = <Text>{value}</Text>;
    }

    return componentValue;
  };

  return (
    <ScrollView>
      <View>{mapping()}</View>
    </ScrollView>
  );
}
