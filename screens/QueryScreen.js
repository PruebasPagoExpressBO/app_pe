import React, {useState} from 'react';
import {Text, View, TextInput, FlatList, Button} from 'react-native';

import axios from 'axios';

export default function QueryScreen({route, navigation}) {
  const {servicio} = route.params;

  const [form, setForm] = useState([]);
  const [formConfirm, setFormConfirm] = useState([]);

  const [inputData, setInputData] = useState([]);

  const [inputs, setInput] = useState([]);

  React.useEffect(() => {
    const fetchData = async () => {
      const response = await axios({
        method: 'post',
        url:
          'http://200.115.16.18:8280/TrexWS/pagossrl_testing/api/servicios/formularios',
        data: {
          servicio: servicio,
        },
        headers: {
          Authorization:
            'Basic cGFnb3Nzcmw6ZDlmMjY3MmEzNDBmMDVlY2YyOWE2MjBmZmZkNzllYjE=',
          'Timestamp-Date': 'Tue, 22 Sep 2020 23:10:45 GMT',
          'Content-Type': 'application/json',
        },
      });

      setForm(response.data.datos.consulta.formulario);
      setFormConfirm(response.data.datos.confirmacion.formulario);
    };

    fetchData();
  }, []);

  function showForm(item) {
    return (
      <TextInput
        placeholder={item.etiqueta}
        name={item.parametro}
        onChangeText={(text) => addValues(item.parametro, text)}
      />
    );
  }

  function addValues(text, index) {
    let dataArray = inputData;
    let checkBool = false;
    if (dataArray.length !== 0) {
      dataArray.forEach((element) => {
        if (element.index === index) {
          console.log('TRUE');
          element.text = text;
          checkBool = true;
        }
      });
    }
    if (checkBool) {
      setInputData(dataArray);
    } else {
      dataArray.push({text: text, index: index});
      setInputData(dataArray);
    }
  }

  const updateInput = (key, value) => {
    // check if the value has been found
    let found = false;
    // temp variable to handle updating the inputs
    // Used due to immutability of inputs
    let temp = inputs;
    // Loop through array
    for (let i = 0; i < temp.length; i += 1) {
      // Check if current object includes the key "key"
      if (Object.keys(temp[i]).includes('key')) {
        // if specific key found, proves that the value has already
        // been added, update value instead of adding new one
        if (temp[i]['key'] === key) {
          // check if value is empty
          // if so, remove key value pair
          if (value === '') {
            temp.splice(i, 1);
            found = true;
            return;
          }
          // Update value associated with given key value pair
          // in object in array
          temp[i]['value'] = value;
          // set found flag to true
          found = true;
          // return to break out of loop
          return;
        }
      }
    }
    // Check if key hasn't been found
    if (!found) {
      // push new key value pair to temp array
      temp.push({key, value});
    }
    // update inputs with temp array
    setInput(temp);
  };

  const mappingInputs = () => {
    // Take array and map individual objects in array
    return form.map((obj) => {
      // Deconstruct key and title to use
      const {parametro, etiqueta} = obj;
      // return components
      return (
        <View>
          <TextInput
            // call updateInput with title and value passed in
            placeholder={etiqueta}
            name={parametro}
            onChangeText={(value) => updateInput(parametro, value)}
          />
        </View>
      );
    });
  };

  const jsonObject = () => {
    let params = "";

    let i = 0;
    while(i < inputs.length - 1){
      params = params + '"' + inputs[i]["key"] + '": "' + inputs[i]["value"] + '",';
      i = i + 1;
    }

    params = params + '"' + inputs[i]["key"] + '": "' + inputs[i]["value"] + '"';

    let jsonParams = '{' + params + '}';

    navigation.navigate("QueryResult", {campos: jsonParams, servicio: servicio, confirmacion: formConfirm});
  };

  return (
    <View>
      <TextInput placeholder="Enter password" />
      {mappingInputs()}
      <Button title="Consultar" onPress={() => jsonObject()}></Button>
    </View>
  );
}
