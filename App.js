/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import ServiceScreen from './screens/ServiceScreen';
import QueryScreen from './screens/QueryScreen';
import QueryResultScreen from './screens/QueryResultScreen';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Service">
        <Stack.Screen
          name="Service"
          component={ServiceScreen}
          options={{headerTitle: 'Servicios'}}
        />
        <Stack.Screen
          name="Query"
          component={QueryScreen}
        />
        <Stack.Screen
          name="QueryResult"
          component={QueryResultScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
